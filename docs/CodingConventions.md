# Coding Conventions #

Several developers contribute to Lightweight Cell Library and we welcome others
who are interested to also contribute to the project. To ensure readability and
consistency in the code, we have adopted the following coding conventions.


## General ##

  + All code must be valid by the C++11 specifications. It must also
    compile with Microsoft Visual Studio 2015.

  + All code contributed to Lightweight Cell Library must be compatible with
    its BSD license.

  + Copyright notices should appear at the top of all source,
    configuration, and text files. The statement should have the following
    form:

    ```
//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================
    ```

  + The CopyrightStatement test checks all files for a similar statement.
    The test will print out a suggested text that can be copied and pasted
    to any file that has a missing copyright statement (with appropriate
    replacement of comment prefix). Exceptions to this copyright statement
    (for example, third-party files with different but compatible
    statements) can be added to LICENSE.txt.

  + All code must compile and run without error or warning messages on the
    nightly dashboards, which include Windows, Mac, and Linux.

  + New code must include regression tests that will run on the dashboards.
    Generally a new class will have an associated "UnitTest" that will test
    the operation of the test directly. There may be other tests necessary
    that exercise the operation with different components or on different
    architectures.

## Style ##

  + Code formatting is strictly enforced. Lightweight Cell Library's development
    workflow includes a reformatting step that compares the formatting of new
    source code with our formatting definitions. Incompatible code can be
    automatically reformatted to be compliant. See [CONTRIBUTING.md -->
    Reformat a Topic][Reformat] for details.

[development workflow]: ../CONTRIBUTING.md#workflow
[reformat]:             ../CONTRIBUTING.md#reformat-a-topic

  + Use only alphanumeric characters in names. Use capitalization to
    demarcate words within a name (camel case).

  + For functions the first word should be in lower case. When the
    function is for previously established conventions such as
    those in the standard library ( `make_*`, `to_string` ) use
    those.

  + Local variables should start in lower case and then use
    camel case.

  + All include files should use include guards. starting right after the
    copyright statement. The naming convention of the include guard macro
    is that it should be all in lower case and start with lcl and than
    continue the path name, starting from the inside the lcl source code
    directory, with non alphanumeric characters, such as / and . replaced
    with underscores. The `#endif` part of the guard at the bottom of the
    file should include the guard name in a comment. For example, the
    lcl/Quad.h header contains the guard

    ```cpp
  #ifndef lcl_Quad_h
  #define lcl_Quad_h
    ```
    at the top and
    ```cpp
  #endif // lcl_Quad_h
    ```
    at the bottom.

## Design ##

  + Do not use `new`, `malloc` or other means of dynamic allocation.
    The calling code should provide all runtime allocations.

  + Do not use the base C integer types like `int`, `long`, etc.
    Use the fixed length types defined in `std::` like `std::int32_t`.

  + All functions and classes in Lightweight Cell Library need be thread safe.

  + In general all classes should be a minimal tag representation of a
    type. Instead of using virtual methods for polymorphic behavior
    Lightweight Cell Library instead uses free functions that have tag parameters.

  + All methods/functions that can be `constexpr` should be marked as such.
    `constexpr` does not guarantee compile-time evaluation; it just guarantees
    that the function can be evaluated at compile time for constant expression
    arguments if the programmer requires it or the compiler decides to
    do so to optimize.

  + Functions and methods shouldn't throw exceptions, and will be marked with
    `noexcept`. Declaring a function noexcept helps optimizers by reducing
    the number of alternative execution paths. It also speeds up the exit
    after failure.

  + All functions and methods need to be marked with `LCL_EXEC` ( `__host__` / `__device__` )
    as the functions need to be usable from CUDA.

  + In general functions writable parameters should be of the form
    `T&` or `T*` so that numerous types like `std::array`, `std::vector`,
    `T[3]` and other that support the `operator[]` are supported.

  + Functions in general should use the return value to represent
    any error state(s).

  + All classes, and member variables, should start with a
    capital letter. Class methods should start with a lower case letter.

  + Always spell out words in names; do not use abbreviations except in
    cases where the shortened form is widely understood and a name in its
    own right (e.g. OpenMP).

  + use `this->` inside of methods when accessing class methods and
    instance variables to distinguish between local variables and instance
    variables.

  + All classes and function parameters should be `const`-correct.

  + Include statements should generally be in alphabetical order. They can
    be grouped by package and type.

  + Non user facing methods should go into an internal `detail` namespace.

  + Namespaces should not be brought into global scope with the `using`
    keyword.


We should note that although these conventions impose a strict statute on
Lightweight Cell Library coding, these rules (other than those involving
licensing and copyright) are not meant to be dogmatic. Examples can be found in
the existing code that break these conventions, particularly when the
conventions stand in the way of readability (which is the point in having
them in the first place).
