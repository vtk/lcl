//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.md for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================
#include <lcl/FieldAccessor.h>

#include <array>
#include <iostream>
#include <vector>

namespace
{

const int readValues[] = { 5, 3, 7, -1, 6, 0, 4, 2, 1, 7, -3, 9 };
const int writeValues[] = { 1, 2, 6, 4, -2, 7, -5, 6, 9, -3, 4, -2 };
constexpr int numValues = static_cast<int>(sizeof(readValues)/sizeof(readValues[0]));
constexpr int maxNumComponents = 4;

template <typename FieldAccessor>
void FieldAccessorReadTest(const FieldAccessor& field, int numTuples)
{
  using ValueType = typename FieldAccessor::ValueType;

  auto data = readValues;
  for (int i = 0; i < numTuples; ++i)
  {
    for (int j = 0; j < field.getNumberOfComponents(); ++j)
    {
      auto exp = static_cast<ValueType>(*data++);
      auto got = field.getValue(i, j);
      if (got != exp)
      {
        std::cout << "Incorrect read value. Expected = " << exp << ", Got = " << got << "\n";
      }
    }
  }

  data = readValues;
  for (int i = 0; i < numTuples; ++i)
  {
    std::array<ValueType, maxNumComponents> tuple;
    tuple.fill(ValueType{});

    field.getTuple(i, tuple);
    for (int j = 0; j < field.getNumberOfComponents(); ++j)
    {
      auto exp = static_cast<ValueType>(*data++);
      auto got = tuple[static_cast<std::size_t>(j)];
      if (got != exp)
      {
        std::cout << "Incorrect read value. Expected = " << exp << ", Got = " << got << "\n";
      }
    }
  }
}

template <typename FieldAccessor>
void FieldAccessorWriteTestValues(const FieldAccessor& field, int numTuples)
{
  using ValueType = typename FieldAccessor::ValueType;

  auto data = writeValues;
  for (int i = 0; i < numTuples; ++i)
  {
    for (int j = 0; j < field.getNumberOfComponents(); ++j)
    {
      field.setValue(i, j, static_cast<ValueType>(*data++));
    }
  }
}

template <typename FieldAccessor>
void FieldAccessorWriteTestTuples(const FieldAccessor& field, int numTuples)
{
  using ValueType = typename FieldAccessor::ValueType;

  auto data = writeValues;
  std::array<ValueType, maxNumComponents> tuple;
  tuple.fill(ValueType{});

  for (int i = 0; i < numTuples; ++i)
  {
    for (int j = 0; j < field.getNumberOfComponents(); ++j)
    {
      tuple[static_cast<size_t>(j)] = static_cast<ValueType>(*data++);
    }
    field.setTuple(i, tuple);
  }
}

template <typename T, size_t NumComponents>
using Vec = std::array<T, NumComponents>;

template <typename T>
struct VecTraits
{
  using ComponentType = T;
  static constexpr int NumComponents = 1;
};

template <typename T, size_t NComps>
struct VecTraits<Vec<T, NComps>>
{
  using ComponentType = T;
  static constexpr int NumComponents = NComps;
};

template<typename T>
void InitRead(std::vector<T>& field)
{
  auto data = readValues;
  auto numTuples = static_cast<size_t>(numValues / VecTraits<T>::NumComponents);
  field.resize(numTuples);
  for (size_t i = 0; i < numTuples; ++i)
  {
    for (int j = 0; j < VecTraits<T>::NumComponents; ++j)
    {
      lcl::component(field[i], j) = static_cast<typename VecTraits<T>::ComponentType>(*data++);
    }
  }
}

template<typename T>
void VerifyWrite(const std::vector<T>& field)
{
  auto data = writeValues;
  auto numTuples = static_cast<size_t>(numValues / VecTraits<T>::NumComponents);
  for (size_t i = 0; i < numTuples; ++i)
  {
    for (int j = 0; j < VecTraits<T>::NumComponents; ++j)
    {
      auto exp = static_cast<typename VecTraits<T>::ComponentType>(*data++);
      auto got = lcl::component(field[i], j);
      if (got != exp)
      {
        std::cout << "Incorrect value written. Expected = " << exp << ", Got = " << got << "\n";
      }
    }
  }
}

template <typename T>
struct FieldArrayHandle
{
  struct Portal
  {
    using ValueType = T;

    const T& Get(int idx) const
    {
      return (*data)[static_cast<size_t>(idx)];
    }

    void Set(int idx, const T& val) const
    {
      (*data)[static_cast<size_t>(idx)] = val;
    }

    std::vector<T> *data;
  };

  Portal GetPortal()
  {
    return Portal{&this->data};
  }

  std::vector<T> data;
};

template <typename PortalType>
class FieldAccessorVtkmPortal
{
public:
  using TupleType = typename PortalType::ValueType;
  using ValueType = typename std::decay<decltype(lcl::component(std::declval<TupleType>(), 0))>::type;

  explicit FieldAccessorVtkmPortal(const PortalType& portal)
    : Portal(portal)
  {
  }

  constexpr int getNumberOfComponents() const
  {
    return VecTraits<TupleType>::NumComponents;
  }

  void setValue(int tuple, int comp, const ValueType& value) const
  {
    auto t = this->Portal.Get(tuple);
    lcl::component(t, comp) = value;
    this->Portal.Set(tuple, t);
  }

  ValueType getValue(int tuple, int comp) const
  {
    return lcl::component(this->Portal.Get(tuple), comp);
  }

  template <typename VecType>
  void setTuple(int tuple, const VecType& value) const
  {
    TupleType t;
    for (int i = 0; i < this->getNumberOfComponents(); ++i)
    {
      lcl::component(t, i) = lcl::component(value, i);
    }
    this->Portal.Set(tuple, t);
  }

  template <typename VecType>
  void getTuple(int tuple, VecType& value) const
  {
    auto t = this->Portal.Get(tuple);
    for (int i = 0; i < this->getNumberOfComponents(); ++i)
    {
      lcl::component(value, i) = lcl::component(t, i);
    }
  }

private:
  PortalType Portal;
};

template <typename T, int NumComponents>
void TestFieldAccessorsOfTypeAndNumComps()
{
  using VecType = typename std::conditional<NumComponents == 1, T, Vec<T, NumComponents>>::type;

  std::cout << "\t\tTesting FieldAccessorFlatSOA\n";

  std::cout << "\t\t\tTesting reads\n";
  std::vector<T> flat;
  InitRead(flat);
  FieldAccessorReadTest(lcl::makeFieldAccessorFlatSOAConst(flat, NumComponents),
                        numValues / NumComponents);

  std::cout << "\t\t\tTesting Writes\n";
  FieldAccessorWriteTestValues(lcl::makeFieldAccessorFlatSOA(flat, NumComponents),
                               numValues / NumComponents);
  VerifyWrite(flat);
  FieldAccessorWriteTestTuples(lcl::makeFieldAccessorFlatSOA(flat, NumComponents),
                               numValues / NumComponents);
  VerifyWrite(flat);


  std::cout << "\t\tTesting FieldAccessorNestedSOA\n";

  std::cout << "\t\t\tTesting reads\n";
  std::vector<VecType> nested;
  InitRead(nested);
  FieldAccessorReadTest(lcl::makeFieldAccessorNestedSOAConst(nested, NumComponents),
                        numValues / NumComponents);

  std::cout << "\t\t\tTesting Writes\n";
  FieldAccessorWriteTestValues(lcl::makeFieldAccessorNestedSOA(nested, NumComponents),
                               numValues / NumComponents);
  VerifyWrite(nested);
  FieldAccessorWriteTestTuples(lcl::makeFieldAccessorNestedSOA(nested, NumComponents),
                               numValues / NumComponents);
  VerifyWrite(nested);


  std::cout << "\t\tTesting custom FieldAccessor for VTK-m stlye portals\n";

  std::cout << "\t\t\tTesting reads\n";
  FieldArrayHandle<VecType> arrayHandle;
  InitRead(arrayHandle.data);

  using FieldAccessorType = FieldAccessorVtkmPortal<decltype(arrayHandle.GetPortal())>;
  FieldAccessorReadTest(FieldAccessorType(arrayHandle.GetPortal()), numValues / NumComponents);

  std::cout << "\t\t\tTesting Writes\n";
  FieldAccessorWriteTestValues(FieldAccessorType(arrayHandle.GetPortal()), numValues / NumComponents);
  VerifyWrite(arrayHandle.data);
  FieldAccessorWriteTestTuples(FieldAccessorType(arrayHandle.GetPortal()), numValues / NumComponents);
  VerifyWrite(arrayHandle.data);
}

template <typename T>
void TestFieldAccessorsOfType()
{
  std::cout << "\tTesting with 1 component\n";
  TestFieldAccessorsOfTypeAndNumComps<T, 1>();

  std::cout << "\tTesting with 2 components\n";
  TestFieldAccessorsOfTypeAndNumComps<T, 2>();

  std::cout << "\tTesting with 3 components\n";
  TestFieldAccessorsOfTypeAndNumComps<T, 3>();

  std::cout << "\tTesting with 4 components\n";
  TestFieldAccessorsOfTypeAndNumComps<T, 4>();
}

void UnitTestFieldAccessors()
{
  std::cout << "Testing type int\n";
  TestFieldAccessorsOfType<int>();
  std::cout << "Testing type float\n";
  TestFieldAccessorsOfType<float>();
}

} // anonymous namespace

int main(int, char*[])
{
  UnitTestFieldAccessors();
}
