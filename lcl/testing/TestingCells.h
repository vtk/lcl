//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.md for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================
#ifndef lcl_testing_TestingCells_h
#define lcl_testing_TestingCells_h

#include <lcl/lcl.h>
#include <lcl/FieldAccessor.h>
#include <lcl/internal/Math.h>

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "internal/TestDataCells.h"

namespace lcl
{
namespace testing
{

//----------------------------------------------------------------------------
template <typename T, int NumComponents>
using Vec = lcl::internal::Vector<T, NumComponents>;

template <typename T, int NumComps>
std::ostream& operator<<(std::ostream& out, const Vec<T, NumComps>& v)
{
  out << "(" << v[0];
  for (int i = 1; i < NumComps; ++i)
  {
    out << ", " << v[i];
  }
  out << ")";
  return out;
}

//----------------------------------------------------------------------------
struct TestData
{
  // cell description
  std::vector<lcl::Cell> Cells;
  std::vector<Vec<float, 3>> Points;

  std::vector<size_t> NumComponents;
  std::vector<double> FieldData;

  // test data
  std::vector<Vec<float, 3>> WCoords;

  // expected results
  std::vector<Vec<float, 3>> PCoords;
  std::vector<float> ParametricDistances;
  std::vector<double> Interpolations;
  std::vector<double> Derivatives;

  // per-cell offsets into the results array
  std::vector<size_t> PointsOffsets;
  std::vector<size_t> FieldDataOffsets;
  std::vector<size_t> InterpolationsOffsets;
};

class TestDataLoader
{
public:
  TestDataLoader()
    : TextStream(std::ios_base::in), LineStream(std::ios_base::in), Done(false)
  {
    this->reset();
  }

  void reset()
  {
    this->TextStream.str(internal::TestDataText);
    this->TextStream.clear();
    this->skip();
  }

  template <typename T>
  size_t loadData(T* buffer, size_t len)
  {
    size_t i = 0;
    for (; !this->Done && i < len; ++i)
    {
      this->LineStream >> buffer[i];
      this->skip();
    }
    return i;
  }

  template <typename T, int NumComponents>
  size_t loadData(Vec<T, NumComponents>* buffer, size_t len)
  {
    size_t i = 0;
    for (; !this->Done && i < len; ++i)
    {
      this->loadData(buffer[i].data(), NumComponents);
    }
    return i;
  }

  bool done() const
  {
    return this->Done;
  }

private:
  void skip()
  {
    while(true)
    {
      this->LineStream >> std::ws;
      if (this->LineStream.eof() || this->LineStream.peek() == '#')
      {
        std::string line;
        std::getline(this->TextStream, line);
        if (this->TextStream.eof())
        {
          this->Done = true;
          break;
        }
        this->LineStream.str(line);
        this->LineStream.clear();
        continue;
      }
      break;
    }
  }

  std::stringstream TextStream;
  std::stringstream LineStream;
  bool Done;
};

TestData LoadTestData()
{
  TestData td;
  TestDataLoader loader;

  std::vector<float> f32Buffer;
  std::vector<double> f64Buffer;
  std::vector<Vec<float, 3>> f32Vec3Buffer;

  while (!loader.done())
  {
    // read cell info
    int info[3];
    loader.loadData(info, 3);

    auto shape = static_cast<int8_t>(info[0]);
    auto numPoints = static_cast<size_t>(info[1]);
    auto numComps = static_cast<size_t>(info[2]);

    td.Cells.push_back(lcl::Cell(shape, static_cast<int32_t>(numPoints)));
    td.NumComponents.push_back(numComps);

    // read the points
    f32Vec3Buffer.resize(numPoints);
    loader.loadData(f32Vec3Buffer.data(), f32Vec3Buffer.size());
    td.PointsOffsets.push_back(td.Points.size());
    td.Points.insert(td.Points.end(), f32Vec3Buffer.begin(), f32Vec3Buffer.end());

    // read field data
    f64Buffer.resize(numPoints * numComps);
    loader.loadData(f64Buffer.data(), f64Buffer.size());
    td.FieldDataOffsets.push_back(td.FieldData.size());
    td.FieldData.insert(td.FieldData.end(), f64Buffer.begin(), f64Buffer.end());

    // read test data: WCoords
    f32Vec3Buffer.resize(internal::NumTestPoints);
    loader.loadData(f32Vec3Buffer.data(), f32Vec3Buffer.size());
    td.WCoords.insert(td.WCoords.end(), f32Vec3Buffer.begin(), f32Vec3Buffer.end());

    auto dim = static_cast<size_t>(lcl::dimension(shape));

    // read test data: PCoords
    f32Buffer.resize(internal::NumTestPoints * dim);
    loader.loadData(f32Buffer.data(), f32Buffer.size());
    for (size_t i = 0; i < internal::NumTestPoints; ++i)
    {
      Vec<float, 3> pc(0.0f, 0.0f, 0.0f);
      for (size_t j = 0; j < dim; ++j)
      {
        pc[static_cast<lcl::IdComponent>(j)] = f32Buffer[i*dim + j];
      }
      td.PCoords.push_back(pc);
    }

    // read test data: ParametricDistances
    f32Buffer.resize(internal::NumOutsideTestPoints);
    loader.loadData(f32Buffer.data(), f32Buffer.size());
    td.ParametricDistances.insert(td.ParametricDistances.end(), f32Buffer.begin(), f32Buffer.end());

    // read test data: Interpolations
    f64Buffer.resize(internal::NumInsideTestPoints * numComps);
    loader.loadData(f64Buffer.data(), f64Buffer.size());
    td.InterpolationsOffsets.push_back(td.Interpolations.size());
    td.Interpolations.insert(td.Interpolations.end(), f64Buffer.begin(), f64Buffer.end());

    // read test data: Derivatives
    f64Buffer.resize(internal::NumInsideTestPoints * numComps * 3);
    loader.loadData(f64Buffer.data(), f64Buffer.size());
    td.Derivatives.insert(td.Derivatives.end(), f64Buffer.begin(), f64Buffer.end());
  }

  // compute parametric coordinates for polygon
  for (size_t i = 0; i < td.Cells.size(); ++i)
  {
    if (td.Cells[i].shape() == lcl::POLYGON)
    {
      for (size_t j = 0; j < internal::NumTestPoints; ++j)
      {
        lcl::worldToParametric(td.Cells[i],
                                lcl::makeFieldAccessorNestedSOAConst(td.Points.data() + td.PointsOffsets[i], 3),
                                td.WCoords[i * internal::NumTestPoints + j],
                                td.PCoords[i * internal::NumTestPoints + j]);
      }
    }
  }

  return td;
}

//----------------------------------------------------------------------------
using BoolType = std::int8_t;

struct TestIsInside
{
  LCL_EXEC void operator()(size_t idx,
                            const lcl::Cell* cells,
                            const Vec<float, 3>* pcoords,
                            BoolType* results) const
  {
    auto cell = cells[idx / internal::NumTestPoints];
    auto pc = pcoords[idx];
    results[idx] = lcl::cellInside(cell, pc);
  }
};

struct TestParametricDistance
{
  LCL_EXEC void operator()(size_t idx,
                            const lcl::Cell* cells,
                            const Vec<float, 3>* pcoords,
                            float* pdists) const
  {
    auto cell = cells[idx / internal::NumTestPoints];
    auto pc = pcoords[idx];
    pdists[idx] = lcl::parametricDistance(cell, pc);
  }
};

struct TestInterpolation
{
  LCL_EXEC void operator()(size_t idx,
                            const lcl::Cell* cells,
                            const double* field,
                            const size_t* numComps,
                            const Vec<float, 3>* pcoords,
                            const size_t* fieldOffsets,
                            const size_t* interpsOffsets,
                            lcl::ErrorCode* codes,
                            double* interps) const
  {
    auto cellIdx = idx / internal::NumInsideTestPoints;
    auto ptIdx = idx % internal::NumInsideTestPoints;

    auto cell = cells[cellIdx];
    auto fptr = field + fieldOffsets[cellIdx];
    auto ncomps = static_cast<lcl::IdComponent>(numComps[cellIdx]);
    auto pc = pcoords[cellIdx * internal::NumTestPoints + ptIdx];
    auto rptr = interps + interpsOffsets[cellIdx] + (ptIdx * numComps[cellIdx]);

    codes[idx] = lcl::interpolate(cell, lcl::makeFieldAccessorFlatSOAConst(fptr, ncomps), pc, rptr);
  }
};

struct TestDerivative
{
  LCL_EXEC void operator()(size_t idx,
                            const lcl::Cell* cells,
                            const Vec<float, 3>* points,
                            const double* field,
                            const size_t* numComps,
                            const Vec<float, 3>* pcoords,
                            const size_t* pointsOffsets,
                            const size_t* fieldOffsets,
                            const size_t* interpsOffsets,
                            lcl::ErrorCode* codes,
                            double* derivs) const
  {
    auto cellIdx = idx / internal::NumInsideTestPoints;
    auto ptIdx = idx % internal::NumInsideTestPoints;

    auto cell = cells[cellIdx];
    auto pts = points + pointsOffsets[cellIdx];
    auto fptr = field + fieldOffsets[cellIdx];
    auto ncomps = static_cast<lcl::IdComponent>(numComps[cellIdx]);
    auto pc = pcoords[cellIdx * internal::NumTestPoints + ptIdx];
    auto rptr = derivs + (interpsOffsets[cellIdx] + (ptIdx * numComps[cellIdx])) * 3;

    codes[idx] = lcl::derivative(cell,
                                  lcl::makeFieldAccessorNestedSOAConst(pts, 3),
                                  lcl::makeFieldAccessorFlatSOAConst(fptr, ncomps),
                                  pc,
                                  rptr + 0,
                                  rptr + ncomps,
                                  rptr + (2 * ncomps));
  }
};

// only accurate for inside points
struct TestWorldToParametric
{
  LCL_EXEC void operator()(size_t idx,
                            const lcl::Cell* cells,
                            const Vec<float, 3>* points,
                            const Vec<float, 3>* wcoords,
                            const size_t* pointsOffsets,
                            lcl::ErrorCode* codes,
                            Vec<float, 3>* pcoords) const
  {
    auto cellIdx = idx / internal::NumInsideTestPoints;
    auto ptIdx = idx % internal::NumInsideTestPoints;

    auto cell = cells[cellIdx];
    auto pts = points + pointsOffsets[cellIdx];
    auto wc = wcoords[cellIdx * internal::NumTestPoints + ptIdx];
    auto& pc = pcoords[idx];

    codes[idx] =
      lcl::worldToParametric(cell, lcl::makeFieldAccessorNestedSOAConst(pts, 3), wc, pc);
  }
};

struct TestParametricToWorld
{
  LCL_EXEC void operator()(size_t idx,
                            const lcl::Cell* cells,
                            const Vec<float, 3>* points,
                            const Vec<float, 3>* pcoords,
                            const size_t* pointsOffsets,
                            lcl::ErrorCode* codes,
                            Vec<float, 3>* wcoords) const
  {
    auto cellIdx = idx / internal::NumTestPoints;

    auto cell = cells[cellIdx];
    auto pts = points + pointsOffsets[cellIdx];
    auto pc = pcoords[idx];
    auto& wc = wcoords[idx];

    codes[idx] =
      lcl::parametricToWorld(cell, lcl::makeFieldAccessorNestedSOAConst(pts, 3), pc, wc);
  }
};

//----------------------------------------------------------------------------
inline LCL_EXEC bool testEqual(float a, float b, float e = 1e-3f)
{
  return (std::abs(a - b) <= e);
}

inline LCL_EXEC bool testEqual(double a, double b, double e = 1e-3f)
{
  return (std::abs(a - b) <= e);
}

inline LCL_EXEC bool testEqual(const float* a, const float* b, size_t len, float e = 1e-3f)
{
  for (size_t i = 0; i < len; ++i)
  {
    if (!testEqual(a[i], b[i], e))
    {
      return false;
    }
  }
  return true;
}

inline LCL_EXEC bool testEqual(const double* a, const double* b, size_t len, double e = 1e-3f)
{
  for (size_t i = 0; i < len; ++i)
  {
    if (!testEqual(a[i], b[i], e))
    {
      return false;
    }
  }
  return true;
}

//----------------------------------------------------------------------------
template <typename T>
void printArray(const char* name, const T* a, size_t numVals, size_t numComps = 1)
{
  if (name)
  {
    std::cout << name << ": ";
  }
  for (size_t j = 0, idx = 0; j < numVals; ++j)
  {
    std::cout << ((j > 0) ? ", " : "");
    std::cout << ((numComps > 1) ? "(" : "");
    for (size_t i = 0; i < numComps; ++i, ++idx)
    {
      std::cout << ((i > 0) ? ", " : "") << a[idx];
    }
    std::cout << ((numComps > 1) ? ")" : "");
  }
  std::cout << "\n";
}

static const std::string Names[] = {
  "Empty",
  "Vertex",
  "Poly-Vertex",
  "Line",
  "Poly-Line",
  "Triangle",
  "Triangle-Strip",
  "Polygon",
  "Pixel",
  "Quad",
  "Tetra",
  "Voxel",
  "Hexahedron",
  "Wedge",
  "Pyramid"
};

template <typename T>
void ReportFailure(const TestData& td,
                   size_t cellIdx,
                   bool showField,
                   size_t testPointIdx,
                   lcl::ErrorCode error,
                   const T* exp,
                   const T* got,
                   size_t numVals,
                   size_t numComps = 1)
{
  std::cout << "\nFailed: ";
  if (error != lcl::ErrorCode::SUCCESS)
  {
    std::cout << lcl::errorString(error);
  }
  std::cout << "\n";

  auto cell = td.Cells[cellIdx];
  auto points = td.Points.data() + td.PointsOffsets[cellIdx];
  auto numPoints = static_cast<size_t>(cell.numberOfPoints());
  std::cout << "Cell: " << Names[cell.shape()] << ", " << cell.numberOfPoints() << "\n";
  printArray("Points", points, numPoints);
  if (showField)
  {
    auto field = td.FieldData.data() + td.FieldDataOffsets[cellIdx];
    printArray("Field", field, numPoints, td.NumComponents[cellIdx]);
  }
  std::cout << "Test Point: WCoord = "
            << td.WCoords[cellIdx * internal::NumTestPoints + testPointIdx]
            << ", PCoord = "
            << td.PCoords[cellIdx * internal::NumTestPoints + testPointIdx]
            << "\n";
  if (error == lcl::ErrorCode::SUCCESS)
  {
    printArray("exp", exp, numVals, numComps);
    printArray("got", got, numVals, numComps);
  }
}

template <typename Executor>
void TestCells(const Executor& executor)
{
  constexpr bool showFieldOn = true;
  constexpr bool showFieldOff = false;

  auto testData = LoadTestData();
  auto numCells = testData.Cells.size();

  std::vector<lcl::ErrorCode> ecodes;

  std::cout << "\n====================================\n";
  std::cout << "Testing lcl::cellInside\n";
  std::vector<BoolType> isInside(numCells * internal::NumTestPoints);
  executor.run(TestIsInside(),
               isInside.size(),
               Executor::in(testData.Cells),
               Executor::in(testData.PCoords),
               Executor::out(isInside));

  for (size_t i = 0, idx = 0 ; i < numCells; ++i)
  {
    // this test doesn't make sense for Vertex
    if (testData.Cells[i].shape() == lcl::VERTEX)
    {
      idx += internal::NumTestPoints;
      continue;
    }
    for (size_t j = 0; j < internal::NumTestPoints; ++j, ++idx)
    {
      bool inside = j < internal::NumInsideTestPoints;
      if (static_cast<bool>(isInside[idx]) != inside)
      {
        std::string exp(inside ? "Inside" : "Outside");
        std::string got(isInside[idx] ? "Inside" : "Outside");
        ReportFailure(testData, i, showFieldOff, j, lcl::ErrorCode::SUCCESS, &exp, &got, 1);
      }
    }
  }

  std::cout << "\n====================================\n";
  std::cout << "Testing lcl::parametricDistance\n";
  std::vector<float> pdists(numCells * internal::NumTestPoints);
  executor.run(TestParametricDistance(),
               pdists.size(),
               Executor::in(testData.Cells),
               Executor::in(testData.PCoords),
               Executor::out(pdists));

  for (size_t i = 0, idx = 0 ; i < numCells; ++i)
  {
    // this test doesn't make sense for Vertex and cannot be verified for Polygon
    if (testData.Cells[i].shape() == lcl::VERTEX ||
        testData.Cells[i].shape() == lcl::POLYGON)
    {
      idx += internal::NumTestPoints;
      continue;
    }
    for (size_t j = 0; j < internal::NumTestPoints; ++j, ++idx)
    {
      float exp = (j < internal::NumInsideTestPoints) ?
                  0.0f :
                  testData.ParametricDistances[(i * internal::NumOutsideTestPoints) + j - internal::NumInsideTestPoints];
      float got = pdists[idx];
      if (!testEqual(got, exp))
      {
        ReportFailure(testData, i, showFieldOff, j, lcl::ErrorCode::SUCCESS, &exp, &got, 1);
      }
    }
  }

  std::cout << "\n====================================\n";
  std::cout << "Testing lcl::interpolate\n";
  ecodes.resize(numCells * internal::NumInsideTestPoints);
  std::vector<double> interpolations(testData.Interpolations.size());
  executor.run(TestInterpolation(),
               ecodes.size(),
               Executor::in(testData.Cells),
               Executor::in(testData.FieldData),
               Executor::in(testData.NumComponents),
               Executor::in(testData.PCoords),
               Executor::in(testData.FieldDataOffsets),
               Executor::in(testData.InterpolationsOffsets),
               Executor::out(ecodes),
               Executor::out(interpolations));

  for (size_t i = 0, idx = 0 ; i < numCells; ++i)
  {
    auto numComps = testData.NumComponents[i];
    for (size_t j = 0; j < internal::NumInsideTestPoints; ++j)
    {
      auto errCode = ecodes[i * internal::NumInsideTestPoints + j];
      auto exp = testData.Interpolations.data() + idx;
      auto got = interpolations.data() + idx;

      if (errCode != lcl::ErrorCode::SUCCESS || !testEqual(exp, got, numComps))
      {
        ReportFailure(testData, i, showFieldOn, j, errCode, exp, got, 1, numComps);
      }
      idx += numComps;
    }
  }

  std::cout << "\n====================================\n";
  std::cout << "Testing lcl::derivative\n";
  ecodes.resize(numCells * internal::NumInsideTestPoints);
  std::vector<double> derivatives(testData.Derivatives.size());
  executor.run(TestDerivative(),
               ecodes.size(),
               Executor::in(testData.Cells),
               Executor::in(testData.Points),
               Executor::in(testData.FieldData),
               Executor::in(testData.NumComponents),
               Executor::in(testData.PCoords),
               Executor::in(testData.PointsOffsets),
               Executor::in(testData.FieldDataOffsets),
               Executor::in(testData.InterpolationsOffsets),
               Executor::out(ecodes),
               Executor::out(derivatives));

  for (size_t i = 0, idx = 0 ; i < numCells; ++i)
  {
    auto numComps = testData.NumComponents[i];
    for (size_t j = 0; j < internal::NumInsideTestPoints; ++j)
    {
      auto errCode = ecodes[i * internal::NumInsideTestPoints + j];
      auto exp = testData.Derivatives.data() + idx;
      auto got = derivatives.data() + idx;

      if (errCode != lcl::ErrorCode::SUCCESS || !testEqual(exp, got, numComps * 3, 0.01))
      {
        ReportFailure(testData, i, showFieldOn, j, errCode, exp, got, numComps, 3);
      }
      idx += numComps * 3;
    }
  }

  std::cout << "\n====================================\n";
  std::cout << "Testing lcl::parametricToWorld\n";
  ecodes.resize(numCells * internal::NumTestPoints);
  std::vector<Vec<float, 3>> p2w(testData.WCoords.size());
  executor.run(TestParametricToWorld(),
               ecodes.size(),
               Executor::in(testData.Cells),
               Executor::in(testData.Points),
               Executor::in(testData.PCoords),
               Executor::in(testData.PointsOffsets),
               Executor::out(ecodes),
               Executor::out(p2w));

  for (size_t i = 0, idx = 0 ; i < numCells; ++i)
  {
    for (size_t j = 0; j < internal::NumTestPoints; ++j, ++idx)
    {
      auto errCode = ecodes[i * internal::NumTestPoints + j];
      auto exp = testData.WCoords[i * internal::NumTestPoints + j];
      auto got = p2w[idx];

      if ((errCode != lcl::ErrorCode::SUCCESS))
      {
        if (lcl::dimension(testData.Cells[i]) == 2)
        {
          auto numPoints = testData.Cells[i].numberOfPoints();
          auto points = testData.Points.data() + testData.PointsOffsets[i];
          auto normal = lcl::internal::normal(
                          lcl::internal::cross(points[numPoints / 3] - points[0],
                                                points[(2 * numPoints) / 3] - points[0]));
          auto dist = lcl::internal::dot((exp - points[0]), normal);
          exp -= dist * normal;
        }

        if (!testEqual(exp.data(), got.data(), 3))
        {
          ReportFailure(testData, i, showFieldOff, j, errCode, &exp, &got, 1);
        }
      }
    }
  }

  // only accurate for inside points
  std::cout << "\n====================================\n";
  std::cout << "Testing lcl::worldToParametric\n";
  ecodes.resize(numCells * internal::NumInsideTestPoints);
  std::vector<Vec<float, 3>> w2p(ecodes.size());
  executor.run(TestWorldToParametric(),
               ecodes.size(),
               Executor::in(testData.Cells),
               Executor::in(testData.Points),
               Executor::in(testData.WCoords),
               Executor::in(testData.PointsOffsets),
               Executor::out(ecodes),
               Executor::out(w2p));

  for (size_t i = 0, idx = 0 ; i < numCells; ++i)
  {
    // this test doesn't make sense for Vertex
    if (testData.Cells[i].shape() == lcl::VERTEX)
    {
      idx += internal::NumInsideTestPoints;
    }
    else
    {
      auto dim = static_cast<size_t>(lcl::dimension(testData.Cells[i]));
      for (size_t j = 0; j < internal::NumInsideTestPoints; ++j, ++idx)
      {
        auto errCode = ecodes[idx];
        auto exp = testData.PCoords[i * internal::NumTestPoints + j];
        auto got = w2p[idx];

        if (errCode != lcl::ErrorCode::SUCCESS || !testEqual(exp.data(), got.data(), dim))
        {
          ReportFailure(testData, i, showFieldOff, j, errCode, &exp, &got, 1);
        }
      }
    }
  }

  std::cout << "\n";
}

}
} // lcl::testing

#endif // lcl_testing_TestingCells_h
