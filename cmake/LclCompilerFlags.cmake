##============================================================================
##  Copyright (c) Kitware, Inc.
##  All rights reserved.
##  See LICENSE.md for details.
##
##  This software is distributed WITHOUT ANY WARRANTY; without even
##  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE.  See the above copyright notice for more information.
##============================================================================

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  set(LCL_COMPILER_IS_GNU 1)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  set(LCL_COMPILER_IS_CLANG 1)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
  set(LCL_COMPILER_IS_CLANG 1)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "PGI")
  set(LCL_COMPILER_IS_PGI 1)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Intel")
  set(LCL_COMPILER_IS_ICC 1)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
  set(LCL_COMPILER_IS_MSVC 1)
endif()

#-----------------------------------------------------------------------------
# lcl_developer_flags is used ONLY BY libraries that are built as part of this
# repository
add_library(lcl_developer_flags INTERFACE)


if(LCL_COMPILER_IS_MSVC)
  target_compile_definitions(lcl_developer_flags INTERFACE "_SCL_SECURE_NO_WARNINGS"
                                                            "_CRT_SECURE_NO_WARNINGS")

  #CMake COMPILE_LANGUAGE doesn't work with MSVC, ans since we want these flags
  #only for C++ compilation we have to resort to setting CMAKE_CXX_FLAGS :(
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4702 /wd4505")
  set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} -Xcompiler=\"/wd4702 /wd4505\" -Xcudafe=\"--diag_suppress=1394 --diag_suppress=766 --display_error_number\"")

  if(MSVC_VERSION LESS 1900)
    # In VS2013 the C4127 warning has a bug in the implementation and
    # generates false positive warnings for lots of template code
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -wd4127")
    set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} -Xcompiler=\"/wd4127\"")
  endif()

elseif(LCL_COMPILER_IS_ICC)
  #Intel compiler offers header level suppression in the form of
  # #pragma warning(disable : 1478), but for warning 1478 it seems to not
  #work. Instead we add it as a definition
  # Likewise to suppress failures about being unable to apply vectorization
  # to loops, the #pragma warning(disable seems to not work so we add a
  # a compile define.
  target_compile_options(lcl_developer_flags INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-wd1478 -wd13379>)

elseif(LCL_COMPILER_IS_GNU OR LCL_COMPILER_IS_CLANG)
  set(cxx_flags -Wall -Wno-long-long -Wcast-align -Wconversion -Wchar-subscripts -Wextra -Wpointer-arith -Wformat -Wformat-security -Wshadow -Wunused-parameter -fno-common)
  set(cuda_flags -Xcudafe=--display_error_number -Xcompiler=-Wall,-Wno-unknown-pragmas,-Wno-unused-local-typedefs,-Wno-unused-local-typedefs,-Wno-unused-function,-Wno-long-long,-Wcast-align,-Wconversion,-Wchar-subscripts,-Wpointer-arith,-Wformat,-Wformat-security,-Wshadow,-Wunused-parameter,-fno-common)

  #GCC 5, 6 don't properly handle strict-overflow suppression through pragma's.
  #Instead of suppressing around the location of the strict-overflow you
  #have to suppress around the entry point, or in vtk-m case the worklet
  #invocation site. This is incredibly tedious and has been fixed in gcc 7
  #
  if(LCL_COMPILER_IS_GNU AND
    (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER 4.99) AND
    (CMAKE_CXX_COMPILER_VERSION VERSION_LESS 6.99) )
    list(APPEND cxx_flags -Wno-strict-overflow)
  endif()
  target_compile_options(lcl_developer_flags
    INTERFACE $<BUILD_INTERFACE:$<$<COMPILE_LANGUAGE:CXX>:${cxx_flags}>>
    )
  if(TARGET vtkm::cuda)
    target_compile_options(lcl_developer_flags
      INTERFACE $<BUILD_INTERFACE:$<$<COMPILE_LANGUAGE:CUDA>:${cuda_flags}>>
      )
  endif()
endif()
